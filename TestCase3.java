import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.Driver;

public class TestCase3 {
    public static void main(String[] args) {

        /*
        Given user navigates to “https://automationexercise.com/”
        Then user should see heading2 as “SUBSCRIPTION”
        And user should see and email input box with “Your email address” placeholder
        And user should see arrow submit button
        And user should see “Get the most recent updates from our site and be updated your self...”
        text under the email input box
         */
        WebDriver driver = Driver.getDriver();

        driver.get("https://automationexercise.com/");

        WebElement heading2 = driver.findElement(By.id("h2"));
        if (heading2.getText().equals("Subscription"))System.out.println("Subscription verification is PASSED");
        else System.out.println( "Subscription verification is FAILED");

        WebElement email = driver.findElement(By.id("susbscribe_email"));
        if (email.isDisplayed() && email.getText().equals("Your email address")) System.out.println("Email address validation is PASSED");
        else System.out.println("Email address validation is FAILED");

        WebElement button = driver.findElement(By.className("fa fa-arrow-circle-o-right"));
        if (button.isDisplayed()) System.out.println("Button validation is PASSED");
        else System.out.println("Button validation is FAILED");

        WebElement text = driver.findElement(By.id("p"));
        if (text.isDisplayed() && text.getText().equals("Get the most recent updates from\n" +
                "our site and be updated your self...")) System.out.println("Text Validation is PASSED");
        else System.out.println("Text Validation is FAILED");

        Driver.quitDriver();
    }
}
