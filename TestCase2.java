import okhttp3.internal.http2.Header;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.Driver;

public class TestCase2 {
        public static void main(String[] args) {
                WebDriver driver = Driver.getDriver();

                driver.get("https://automationexercise.com/");

                WebElement heading = driver.findElement(By.xpath("//*[@id=\"col-sm-8\"]"));

                if(heading.isDisplayed() && heading.isEnabled()) System.out.println("Automation Exercise Heading validation is PASSED");
                else System.out.println("Automation Exercise Heading validation is FAILED!!!");

                WebElement home = driver.findElement(By.xpath("//a[@href='/fa-home']"));
                if (home.isEnabled() && home.isDisplayed()) System.out.println("\"Automation Exercise Home validation is PASSED\"");
                else System.out.println("\"Automation Exercise Home validation is failed\"");


                WebElement products = driver.findElement(By.xpath("//a[@href=\"/products\"']"));
                if(products.isDisplayed()) System.out.println("Products validation is PASSED");
                else System.out.println("Products  validation is FAILED!!!");

                WebElement cart = driver.findElement(By.xpath("//a[@href=\"/view_cart\""));

                if(cart.isDisplayed() && cart.isEnabled()) System.out.println("Cart validation validation is PASSED");
                else System.out.println("Cart validation is FAILED");

                WebElement signup = driver.findElement(By.xpath("//a[@href=\"/login\""));
                if(signup.isDisplayed() && signup.isEnabled()) System.out.println("Signup/Login validation is PASSED");
                else System.out.println("Singup/Login validation is FAILED!!!");

                WebElement testcases = driver.findElement(By.xpath("//a[@href=\"/test_cases\""));
                if (testcases.isEnabled() && testcases.isDisplayed()) System.out.println("TestCases validation is PASSED");
                else System.out.println("TestCases Validation is FAILED");

                WebElement apiTesting = driver.findElement(By.xpath("//a[@href=\"/api_list\""));
                if (apiTesting.isDisplayed() && apiTesting.isEnabled()) System.out.println("API Testing validation is PASSED");
                else System.out.println("API testing validation FAILED");

                WebElement videoTutorials = driver.findElement(By.xpath("//a[@href=\"https://www.youtube.com/c/AutomationExercise\""));
                if (videoTutorials.isEnabled() && videoTutorials.isDisplayed()) System.out.println("Video Tutorials validation PASSED");
                else System.out.println("Video Tutorials validation FAILED");

                WebElement contactUs = driver.findElement(By.xpath("//a[@href=\"/contact_us\""));
                if (contactUs.isDisplayed() && contactUs.isEnabled()) System.out.println("Contact Us valdation is PASSED");
                else System.out.println("Contact Us validation is FAILED");
                Driver.quitDriver();
        }
}
