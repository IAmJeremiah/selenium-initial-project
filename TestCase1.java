import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.Driver;

public class TestCase1 {
    public static void main(String[] args) {
        WebDriver driver = Driver.getDriver();

        driver.get("https://automationexercise.com/");

        WebElement logoImage = driver.findElement(By.xpath("//*[@id=\"col-sm-4\"]"));

        if(logoImage.isDisplayed()) System.out.println("Automation Exercise Logo validation is PASSED");
        else System.out.println("Automation Exercise Logo validation is FAILED!!!");

        Driver.quitDriver();;
    }
}
